﻿using System;

namespace ÖdipusimLabyrinth
{
    internal class ConsoleResultWriter : IResultWriter
    {
        public void Write(string resultstr)
        {
            Console.WriteLine(resultstr);
        }
    }
}