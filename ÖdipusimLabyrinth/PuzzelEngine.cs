﻿using System;

namespace ÖdipusimLabyrinth
{
    internal sealed class PuzzelEngine
    {
        private readonly ILabyrinthParameter _parameter; 
        internal PuzzelEngine(ILabyrinthParameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException(nameof(parameter));
            if (parameter.Steine == null)
                throw new ArgumentException("parameter.Steine");
            if (parameter.LabyrinthArray == null)
                throw new ArgumentException("parameter.LabyrinthArray");
            if (parameter.ResultWriter == null)
                throw new ArgumentException("parameter.ResultWriter");
            _parameter = parameter; 
        }
        internal void GeheZu(int x, int y)
        {
            if (IstUngueltig(x, y) || IstSchonBesucht(x, y)) { return; }
            Sammle(x, y);
            MarkiereAlsBesucht(x, y);
            GeheZu(x + 1, y); //nach rechts
            GeheZu(x, y + 1); //nach unten
            GeheZu(x - 1, y); //nach links
            GeheZu(x, y - 1); //nach oben
        }
        private bool IstUngueltig(int x, int y)
        {
            if (x < 0 || x > 6) { return true; }
            if (y < 0 || y > 6) { return true; }
            if (_parameter.LabyrinthArray[x, y] == _parameter.HindernisMarker) { return true; }

            return false;
        }
        private bool IstSchonBesucht(int x, int y)
        {
            return _parameter.LabyrinthArray[x, y] == _parameter.SchonbesuchtMarker;
        }
        private void Sammle(int x, int y)
        {
            if (_parameter.Steine.Contains(_parameter.LabyrinthArray[x, y]))
                Console.WriteLine("Gesammlet: " + x + ", " + y + ": " + _parameter.LabyrinthArray[x, y]);
        }
        private void MarkiereAlsBesucht(int x, int y)
        {
            _parameter.LabyrinthArray[x, y] = _parameter.SchonbesuchtMarker;
        }
    }
}
