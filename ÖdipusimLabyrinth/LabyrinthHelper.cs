﻿using System.Collections.Generic;

namespace ÖdipusimLabyrinth
{
    internal static class LabyrinthHelper
    {
        internal static ILabyrinthParameter DefaultParameter()
        {
            return new LabyrinthParameter
            {
                HindernisMarker = 'H',
                SchonbesuchtMarker = 'B',
                LabyrinthArray = DefaultLabyrinthArray(),
                Steine = DefaultSteine,
                ResultWriter = new ConsoleResultWriter()
            };
        }
        internal static char[,] DefaultLabyrinthArray()
        {
            var labyrinth = CreateArray();
            //Startposition Ödipus: (3, 2);
            //Buchstaben: L(4, 1); A(6, 2); I(2, 4); O(2, 1); S(1, 3);
            labyrinth[4, 1] = 'L';
            labyrinth[6, 2] = 'A';
            labyrinth[2, 4] = 'I';
            labyrinth[2, 1] = 'O';
            labyrinth[1, 3] = 'S';
            //Hindernisse: (0, 1); (1, 4); (2, 2); (3, 6); (6, 1); (6, 3);
            labyrinth[0, 0] = labyrinth[0, 1] 
                            = labyrinth[1, 4] 
                            = labyrinth[2, 2] 
                            = labyrinth[3, 6] 
                            = labyrinth[6, 1] 
                            = labyrinth[6,3]
                            ='H';
            return labyrinth;
        }
        internal static char[,] CreateArray(int length = 7, char defaultvalue = '0')
        {
            char[,] labyrinth = new char[length, length];
            for (var i = 0; i < length; ++i)
                for (var j = 0; j < length; j++)
                    labyrinth[i, j] = defaultvalue;
            return labyrinth;
        }
        internal static readonly List<char> DefaultSteine = new List<char> { 'L', 'A', 'I', 'O', 'S' };
    }
}
