﻿using System.Collections.Generic;

namespace ÖdipusimLabyrinth
{
    internal class LabyrinthParameter : ILabyrinthParameter
    {
        public char HindernisMarker { get; set; }
        public char[,] LabyrinthArray { get; set; }
        public char SchonbesuchtMarker { get; set; }
        public List<char> Steine { get; set; }
        public IResultWriter ResultWriter { get; set; }
    }
}