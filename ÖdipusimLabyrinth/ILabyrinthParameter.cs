﻿using System.Collections.Generic;

namespace ÖdipusimLabyrinth
{
    interface ILabyrinthParameter
    {
        char[,] LabyrinthArray { get; }
        List<char> Steine { get; }
        char HindernisMarker { get; }
        char SchonbesuchtMarker { get; }
        IResultWriter ResultWriter { get; } 
    }
}
